using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform turretTranform;
    public Transform cursorHighlight;
    public VariableJoystick rotateJoystick;
    public VariableJoystick moveJoystick;
    RaycastHit hit;
    public Rigidbody rb;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        HandleRotate(1);
        HandleMovement();
    }


    void HandleRotate(float delta) {
        // Ray ray = Camera.main.ScreenPointToRay(variableJoystick.Vertical + variableJoystick.Horizontal);
        //turretTranform.localRotation = turretTranform.localRotation * Quaternion.Euler(new Vector3(0, variableJoystick.Vertical * 10, 0));
        turretTranform.eulerAngles = new Vector3(0, (Mathf.Atan2(rotateJoystick.Vertical, rotateJoystick.Horizontal) * 180 / Mathf.PI), 0);

       // if (Physics.Raycast(ray, out hit, 50000.0f)) {

        //    cursorHighlight.position = hit.point;
        //    Vector3 rotation = Quaternion.LookRotation(hit.point - turretTranform.position).eulerAngles;
        //    turretTranform.localRotation = Quaternion.Euler(new Vector3(0, rotation.y, 0));
        // }

    }


    void HandleMovement() {
        Vector3 moveDir = transform.right * moveJoystick.Horizontal + transform.forward * moveJoystick.Vertical;
        rb.AddForce(moveDir * 5000 * Time.deltaTime);


    }
}
