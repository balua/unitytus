using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;

public class SFS2X_Connect : MonoBehaviour
{
    public string ConfigFile = "_Scripts/_Network/sfs-config.xml";
    public bool UserConfigFile = false;
    public string ServerIp = "127.0.0.1";
    public int ServerPort = 9933;
    public string ZoneName = "PvPZone";
    public string RoomName = "The Lobby";
    public string UserName = "";
    public string Password = "";


    SmartFox sfs;



    void Start()
    {
        sfs = new SmartFox();
        sfs.ThreadSafeMode = true;
        sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
        sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
        sfs.AddEventListener(SFSEvent.CONFIG_LOAD_SUCCESS, OnConfigLoad);
        sfs.AddEventListener(SFSEvent.CONFIG_LOAD_FAILURE, OnConfigError);
        sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnJoinRoom);
        sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnJoinRoomError);
        sfs.AddEventListener(SFSEvent.PUBLIC_MESSAGE, onPublicMessage);
        sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);

        if (UserConfigFile)
        {
            sfs.LoadConfig(Application.dataPath + "/" + ConfigFile, true);

        }
        else {
            sfs.Connect(ServerIp, ServerPort);
        }



    }


    void OnConfigLoad(BaseEvent e) {

        Debug.Log("Config File Loaded");
        sfs.Connect(sfs.Config.Host, sfs.Config.Port);
    }


    void OnConfigError(BaseEvent e)
    {
        Debug.Log("Failed to Load Config File: ");
    }

    public string DictionaryToString(Dictionary<string, object> dictionary)
    {
        string dictionaryString = "{";
        foreach (KeyValuePair<string, object> keyValues in dictionary)
        {
            dictionaryString += keyValues.Key + " : " + keyValues.Value + ", ";
        }
        return dictionaryString.TrimEnd(',', ' ') + "}";
    }


    void OnConnection(BaseEvent e) {
        if ((bool)e.Params["success"])
        {
            Debug.Log("Successfully Connected");
            sfs.Send(new LoginRequest(UserName, Password, ZoneName));


        }
        else {
            print("Connection Failed");
        }

    }

    void OnConnectionFailed(BaseEvent e) {


    }



    void OnLogin(BaseEvent e)
    {

        Debug.Log("Logged in: " + e.Params["user"]);
        //sfs.Send(new JoinRoomRequest(RoomName));

       
        ISFSObject objOut = new SFSObject();

        sfs.Send(new ExtensionRequest("roomList", objOut));
        
        
    }


    void OnExtensionResponse(BaseEvent e) {
        string cmd = (string)e.Params["cmd"];
        ISFSObject objIn = (SFSObject)e.Params["params"];
        Debug.Log("cmd : " + cmd);
        if (cmd == "roomList") {
            Debug.Log("rooms: " + objIn.GetUtfString("rooms"));
        
        }

    }

    void OnLoginError(BaseEvent e)
    {
        Debug.Log("Login error: " + e.Params["errorMessage"]);

    }



    void OnJoinRoom(BaseEvent e)
    {
        Debug.Log("Joined Room: " + e.Params["room"]);
        sfs.Send(new PublicMessageRequest("Hello World!!!"));
    }

    void onPublicMessage(BaseEvent e) {
        Room room = (Room)e.Params["room"];
        User sender = (User)e.Params["sender"];
        Debug.Log("[" + room.Name + "] " + sender.Name + ": " + e.Params["message"]);
    }

    void OnJoinRoomError(BaseEvent e)
    {
        Debug.Log("JoinRoom Error (" + e.Params["errorCode"] + "): " + e.Params["errorMessage"]);
    }

    // Update is called once per frame
    void Update()
    {
        if (sfs != null) {
            sfs.ProcessEvents();
        }
    }
}
